﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.RichEdit;
using DevExpress.XtraRichEdit;

namespace Lab2_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private string currentFileName = string.Empty;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void NewDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            richEditControl1.CreateNewDocument();
            richEditControl1.IsEnabled = true;
            SaveDocumentButton.IsEnabled = true;
            CloseDocumentButton.IsEnabled = true;
        }

        private void CloseDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            richEditControl1.IsEnabled = false;
            richEditControl1.Text = "";
            SaveDocumentButton.IsEnabled = false;
            CloseDocumentButton.IsEnabled = false;


            var tempFilePath = string.Format("{0}\\~{1}",
                currentFileName.Split('\\').First(),
                currentFileName.Split('\\').Last());
            if (File.Exists(tempFilePath))
            {
                File.Delete(tempFilePath);
            }
            currentFileName = string.Empty;
        }

        private void SaveDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            using (var myFileDialog = new SaveFileDialog())
            {
                myFileDialog.Filter = "Text files (*.txt)|*.txt";
                myFileDialog.FilterIndex = 1;
                myFileDialog.InitialDirectory = "D:\\";
                myFileDialog.Title = "Save File";
                myFileDialog.CheckFileExists = false;
                DialogResult result = myFileDialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    richEditControl1.SaveDocument(myFileDialog.FileName, DocumentFormat.PlainText);
                }
            }
        }

        private void LoadDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            var tempFilePath = string.Format("{0}\\~{1}", 
                currentFileName.Split('\\').First(),
                currentFileName.Split('\\').Last());
            if (File.Exists(tempFilePath))
            {
                File.Delete(tempFilePath);
            }
            using (var myFileDialog = new OpenFileDialog())
            {
                myFileDialog.Filter = "Text files (*.txt)|*.txt";
                myFileDialog.FilterIndex = 1;
                myFileDialog.InitialDirectory = "D:\\";
                myFileDialog.Title = "Open File";
                myFileDialog.CheckFileExists = false;
                DialogResult result = myFileDialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    if (ValidateFileNotUsed(myFileDialog.FileName))
                    {
                        tempFilePath = string.Format("{0}\\~{1}",
                                myFileDialog.FileName.Split('\\').First(),
                                myFileDialog.FileName.Split('\\').Last());
                        using (Stream s = File.OpenWrite(tempFilePath))
                        {
                            using (var sw = new StreamWriter(s))
                            {
                                sw.WriteLine(Process.GetCurrentProcess().Id.ToString());
                                File.SetAttributes(tempFilePath, FileAttributes.Hidden);
                            }
                        }
                        richEditControl1.LoadDocument(myFileDialog.FileName, DocumentFormat.PlainText);
                        currentFileName = myFileDialog.FileName;
                        SaveDocumentButton.IsEnabled = true;
                        CloseDocumentButton.IsEnabled = true;
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("File is already used by another process."); 
                    }
                }
            }
            //richEditControl1.LoadDocument(@"D:\Test\document.txt", DocumentFormat.PlainText);
        }
        private bool ValidateFileNotUsed(string filePath)
        {
            var tempFileName = string.Format("{0}\\~{1}", filePath.Split('\\').First(), filePath.Split('\\').Last());
            if (File.Exists(tempFileName))
            {
                using (var s = new StreamReader(tempFileName))
                {
                    string pid = s.ReadLine();
                    if (Process.GetProcessById(Int32.Parse(pid)) != null)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
    
}
