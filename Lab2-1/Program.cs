﻿using System;
using System.Threading;
using SpreadsheetLight;

namespace Lab2_1
{
    class Program
    {
        static void Main()
        {
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    var thread = new Thread(OpenSampleExcel);
                    thread.Start();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        static void OpenSampleExcel()
        {
            using (var doc = new SLDocument("D:\\sample.xlsx"))
            {
                doc.SaveAs("D:\\sample1.xlsx");
            }
        }
    }
}
